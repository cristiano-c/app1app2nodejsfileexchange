// http://ejohn.org/blog/ecmascript-5-strict-mode-json-and-more/
"use strict";

const log = require('ololog').configure({time: true});

const MANUFACTURER_HOST = "localhost";
const MANUFACTURER_PORT = "8080";


var request = require("request");

// Optional. You will see this name in eg. 'ps' or 'top' command
process.title = 'node-prototype';

// Port where we'll run the websocket server
var webSocketsServerPort = 1337;
var httpListeningPort = 8000;

let fs = require('fs');

// websocket and http servers
var webSocketServer = require('websocket').server;
var http = require('http');

//var database = require('./database');
//var deployer = require('./deploy');

var finalhandler = require('finalhandler');
var serveStatic = require('serve-static');
var multiparty = require('multiparty');
var Multer = require('multer');
var multer = Multer({ dest: '/Users/sahelanthropus/IdeaProjects/ThesisPrototype/prototype-local/files' });
var util = require('util');

var serve = serveStatic("./");

/**
 * Global variables
 */
// latest 100 messages
var history = [];
// list of currently connected clients (users)
var clients = [];


var parameters;

/**
 * HTTP server (used only to load the HTML initial page for user GUI
 */
var server = http.createServer(function (request, response) {
    if (request.url === '/') request.url = '/client.html';
    var done = finalhandler(request, response);
    serve(request, response, done);
}).listen(webSocketsServerPort, function () {
    log.cyan("CompanyApp frontend is listening on 👂 http://localhost:" + webSocketsServerPort + " (Websockets)");
});



var connection;
var contractAddress = null;


/**
 * HTTP server (used only to communicate with the ManufacturerApp)
 */
var express = require('express');
var httpServer = express();


/**
 * WebSocket server
 */
var wsServer = new webSocketServer({
    // WebSocket server is tied to a HTTP server. WebSocket request is just
    // an enhanced HTTP request. For more info http://tools.ietf.org/html/rfc6455#page-6
    httpServer: server
});

httpServer.listen(httpListeningPort, function () {
    log.cyan('CompanyApp backend is listening on 👂 http://localhost:' + httpListeningPort + " (HTTP)");
});

/**
 * API HTTP
 */

httpServer.get('/', function (req, res) {
    res.send('Hello World!');
});

httpServer.post('/ping', function (req, res) {
    log.yellow("HTTP ping received");
    res.send('Hello World!');
    res.end();
    //deployer.subscribeToBlockchainEvents(connection);
});

httpServer.post('/api/printDone', function (req, res) {
    log.blue.underline("🎉 Print done! Transaction hash: " + req.query.transactionHash);
    res.send('Thank you!');
    res.end();
    let printLogJson = req.query.printLogJson;

    //log.red(util.inspect(req));
    //fs.writeFileSync('/Users/sahelanthropus/IdeaProjects/ThesisPrototype/prototype-local/req.txt', util.inspect(req, true, 20));

    var form = new multiparty.Form();

    form.parse(req, function(err, fields, files) {
        log.red("err " + util.inspect(err));
        log.red("fields " + util.inspect(fields));
        log.red("files " + util.inspect(files));
    });

    // TODO salvare il file json del print log in MondoDB...
    //sendData({type: "newState", state: {type: "4", transactionHash: req.query.transactionHash, printLogJson: printLogJson}});
});



function sendGcodeToManufacturerApp(data) {

    var options = {
        method: 'POST',
        url: 'http://' + MANUFACTURER_HOST + ':' + MANUFACTURER_PORT + '/api/print',
        headers:
            {
                'cache-control': 'no-cache',
                'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
            },
        formData:
            {
                file:
                    {
                        value: fs.createReadStream("./tmp.gcode"),
                        options:
                            {
                                filename: './'+data.gcodeName,
                                contentType: null
                            }
                    },
                contractAddress: data.contractAddress,
                gcodeName: data.gcodeName,
                partNumber: data.partNumber,
            }
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        log.green("✈️ GCODE file of the part number ["+data.partNumber+"] has been sent to ManufacturerApp via HTTP");
    });

}


/*

if (message.type === 'utf8') { // accept only text
    if (userName === false) { // first message sent by user is their name
        // remember user name
        userName = htmlEntities(message.utf8Data);
        // get random color and send it back to the user
        userColor = colors.shift();
        connection.sendUTF(JSON.stringify({ type:'color', data: userColor }));


    } else { // log and broadcast the message


        // we want to keep history of all sent messages
        var obj = {
            time: (new Date()).getTime(),
            text: htmlEntities(message.utf8Data),
            author: userName,
            color: userColor
        };
        history.push(obj);
        history = history.slice(-100);

        // broadcast message to all connected clients
        var json = JSON.stringify({ type:'message', data: obj });
        for (var i=0; i < clients.length; i++) {
            clients[i].sendUTF(json);
        }
    }
} else if (message.type === 'json') {
    log("received a JSON Object: " + message);
} else {
    log("Unknown message type: " + message);
}

*/
