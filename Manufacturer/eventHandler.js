const web3 = require('web3');
const express = require('express');
const Tx = require('ethereumjs-tx');
const fs = require('fs');
const request = require('request');
const app = express();
const ip = require('ip');
const formidable = require('formidable');
const http = require('http');
const util = require('util');
const net = require('net');
const log = require('ololog').configure({time: true});
const busboy = require('connect-busboy'); //middleware for form/file upload
const path = require('path');     //used for file path
const LISTENING_PORT = 8080;
const OCTOPRINT_PORT = 5000; //80;
const {Keccak} = require('sha3');
const hash = new Keccak(256);
const OCTOPRINT_HOST = "localhost"; //"130.192.5.236";
const OCTOPRINT_USER = "xxx"; // FIXME
const OCTOPRINT_PASS = "xxx"; // FIXME
const INFURA_HTTPS_TOKEN = "https://ropsten.infura.io/v3/xxx"; // FIXME
const OCTOPRINT_API_KEY = "xxx"; // FIXME
const MANUFACTURER_LOG_FOLDER = "/Users/sahelanthropus/IdeaProjects/ThesisPrototype/ManufacturerApp/logs/";
const OCTOPRINT_GCODES_FOLDER = "/Users/sahelanthropus/Library/Application Support/OctoPrint/uploads/";
const PRINT_LOG_JSON_FILE = MANUFACTURER_LOG_FOLDER + 'print.log.json';
const COMPANY_HOST = "localhost";
const COMPANY_PORT = "8000";
var ORIGINAL_IP = null;
//const blockchain = require('./blockchain-functionalities');
//const MyTX = require('./transactFunctionCall');

// Used to write console.log on the specified file instead of the console screen
//var access = fs.createWriteStream(MANUFACTURER_LOG_FOLDER + 'events.log');
//process.stdout.write = process.stderr.write = access.write.bind(access);

const SHARED_CONTRACT_ADDRESS = "/Users/sahelanthropus/IdeaProjects/ThesisPrototype/ManufacturerApp/shared/data.json";
const SHARED_PART_NUMBER = "/Users/sahelanthropus/IdeaProjects/ThesisPrototype/ManufacturerApp/shared/partNumber.json";
const SHARED_GCODE_NAME = "/Users/sahelanthropus/IdeaProjects/ThesisPrototype/ManufacturerApp/shared/gcodeName.json";

const EVENT_TYPE = process.argv[2];
const PRINT_START_TIME = process.argv[3];
const PRINT_FILE_NAME = process.argv[4];
const contractAddress = JSON.parse(fs.readFileSync(SHARED_CONTRACT_ADDRESS).toString()).contractAddress;


function sendFileToCompany(filepath) {
    var options = { method: 'POST',
        url: 'http://localhost:8000/api/printDone',
        qs: { transactionHash: '123erfcde456yuhgyt678ik' },
        headers:
            {
                'cache-control': 'no-cache',
                'Access-Control-Allow-Headers': 'Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, Accept-Encoding, X-GitHub-OTP, X-Requested-With, User-Agent',
                'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' },
        formData:
            { file:
                    { value: fs.createReadStream(filepath),
                        options:
                            { filename: filepath,
                                contentType: null } } } };

    console.log(options);

    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        console.log(body);
    });
}

sendFileToCompany('/Users/sahelanthropus/IdeaProjects/NodeJSFileExchange/Manufacturer/data.json');